# Atelier Kubernetes


## Installation de MongoDB

Nous avons l'outil Helm et pour savoir ce qu'il fait on peut visiter le site internet ou le man : <https://helm.sh/docs/helm/helm/>

```console
    user@user:~$ man helm
```

>Où trouver le repo de MongoDB ? Comment va-t-on le déclarer à notre cluster ?

```console
    $ helm repo add bitnami https://charts.bitnami.com/bitnami
    $ helm search repo bitnami
```
>Comment peut-on templater notre déploiement ?

``` {.yaml language="yaml"}
    architecture: replicaset
    replicaCount: 1
    auth:
        rootPassword: secret-root-pwd

```

```console
    $ helm install mongodb --values test-mongodb.yaml bitnami/mongodb
```

## Déploiement de MongoExpress

>Déploiement Classique avec une configuration à la main dans un YAML, on peut remarquer ici la différence entre les deux façons de faire.

``` {.yaml language="yaml"}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongo-express
  labels:
    app: mongo-express
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mongo-express
  template:
    metadata:
      labels:
        app: mongo-express
    spec:
      containers:
      - name: mongo-express
        image: mongo-express
        ports: 
        - containerPort: 8081
        env:
        - name: ME_CONFIG_MONGODB_ADMINUSERNAME
          value: root
        - name: ME_CONFIG_MONGODB_SERVER
          value: mongodb-0.mongodb-headless
        - name: ME_CONFIG_MONGODB_ADMINPASSWORD
          valueFrom: 
            secretKeyRef:
              name: mongodb
              key: mongodb-root-password
---
apiVersion: v1
kind: Service
metadata:
  name: mongo-express-service
spec:
  selector:
    app: mongo-express
  ports:
    - protocol: TCP
      port: 8081
      targetPort: 8081
```

## Déploiement de notre Ingress Controller
>Afin d'accéder à notre service on créé une Ingress afin de pouvoir y accéder depuis l'exterieur.

```console
$ helm repo add nginx-stable https://helm.nginx.com/stable
$ helm repo update
$ helm install my-release nginx-stable/nginx-ingress
$ kubectl delete service traefik -n kube-system
```

## Déploiement de notre Ingress
>On vient créer notre Ingress Rule pour rediriger notre traffic vers notre service.

```{.yaml language="yaml"}
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: mongo-express
spec:
  rules:
    - host: www.example.com
      http:
        paths:
          - path: /
            backend:
              serviceName: mongo-express-service
              servicePort: 8081
```
